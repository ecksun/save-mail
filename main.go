package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"mime"
	"net/mail"
	"os"
	"path"
	"strings"
	"time"
	"unicode"
)

func printHelp() {
	fmt.Printf(`Usage: %s MSG [FOLDER]

Save the message to a file with the date and subject of the email. For example:
2024-02-28 - This is the email subject.eml

If MSG is - read from stdin

If FOLDER is defined write the message to that folder, otherwise use the current directory
`, os.Args[0])
}

func mailReader() io.Reader {
	file := os.Args[1]
	if file == "-" {
		return os.Stdin
	} else {
		f, err := os.Open(os.Args[1])
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to read file: %s\n", err)
			os.Exit(1)
		}
		return f
	}
}

func main() {
	var r io.Reader
	outDir := "." // Current directory

	if len(os.Args) == 1 || os.Args[1] == "--help" || os.Args[1] == "-h" {
		printHelp()
		os.Exit(0)
	}
	if len(os.Args) == 2 { // $0 MSG
		r = mailReader()
	} else if len(os.Args) == 3 { // $0 MSG FOLDER
		r = mailReader()
		outDir = os.Args[2]
	} else {
		fmt.Fprintf(os.Stderr, "Too many arguments")
		printHelp()
		os.Exit(1)
	}

	rawMail, err := io.ReadAll(r)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to read file: %s\n", err)
		os.Exit(1)
	}

	m, err := mail.ReadMessage(bytes.NewReader(rawMail))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to parse email: %s\n", err)
		os.Exit(1)
	}
	subject := m.Header.Get("Subject")
	dec := new(mime.WordDecoder)
	sub, err := dec.DecodeHeader(subject)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to parse subject: %s\n", err)
		os.Exit(1)
	}
	safeSub := strings.Map(func(r rune) rune {
		if unicode.IsPrint(r) {
			return r
		}
		return -1
	}, sub)

	date, err := m.Header.Date()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to read date header (%s), skipping\n", err)
	}
	filename := fmt.Sprintf("%s - %s.eml", date.Format(time.DateOnly), safeSub)
	if date.IsZero() {
		filename = fmt.Sprintf("%s.eml", safeSub)
	}
	resPath := path.Join(outDir, filename)

	if _, err := os.Stat(resPath); !errors.Is(err, os.ErrNotExist) {
		fmt.Fprintf(os.Stderr, "File already exists: %q\n", resPath)
		os.Exit(1)
	}

	if err := os.WriteFile(resPath, rawMail, 0666); err != nil {
		fmt.Fprintf(os.Stderr, "Failed to write to %q: %s\n", resPath, err)
		os.Exit(1)
	}
}
