# save-mail

This is a small tool to save emails in a format I prefer

## Installation

```bash
GOBIN=$HOME/bin go install codeberg.org/ecksun/save-mail@latest
```

## Usage (mutt)

```
macro index Sd "| save-mail - $HOME/Downloads/\n"
```
